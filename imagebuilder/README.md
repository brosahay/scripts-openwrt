# INSTALLATION

## Run the docker development container
```sh
docker-compose up -d
```

This will do the following:
 - create docker image if it is not present
 - create docker volume for persistent storage of the openWRT imagebuilder.
 - first time it will download the openWRT imagebuilder.

# CONFIGURATION
Modify the `VERSION` in `entrypoint.sh` in order to build some other version of openWRT.

```sh
...
VERSION="22.03.3"
...
```
Remove the volume and rebuild the docker image
```sh
docker build -t openwrt-imagebuilder:latest .
```
