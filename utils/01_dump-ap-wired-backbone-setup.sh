#!/bin/bash

# ========================================================
# Setup a Dumb AP, Wired backbone for OpenWRT / LEDE
# ========================================================
# Set lan logical interface as bridge (to allow bridge multiple physical interfaces)
uci set network.lan.type='bridge'
# assign WAN physical interface to LAN (will be available as an additional LAN port now)
uci set network.lan.device="$(uci get network.lan.device) $(uci get network.wan.device)"
uci del network.wan.device
# Remove wan logical interface, since we will not need it.
uci del network.wan

# Disable Dnsmasq completely (it is important to commit or discard dhcp)
uci commit dhcp; echo '' > /etc/config/dhcp
/etc/init.d/dnsmasq disable
/etc/init.d/dnsmasq stop

# Set DHCP on LAN (not recommended, but useful when Dumb AP is moveable from one building to another)
uci del network.lan.broadcast
uci del network.lan.dns
uci del network.lan.gateway
uci del network.lan.ipaddr
uci del network.lan.netmask
uci set network.lan.proto='dhcp'

# To identify better when connected to SSH and when seen on the network
uci set system.@system[0].hostname=''
uci set network.lan.hostname='$(uci get system.@system[0].hostname)'
uci set system.@system[0].zonename='Asia/Kolkata'
uci set system.@system[0].timezone='IST-5:30'

# ========================================================
# Optional, Disable IPv6
# ========================================================
uci del network.lan.ip6assign
uci set network.lan.delegate='0'
uci del dhcp.lan.dhcpv6
uci del dhcp.lan.ra
uci del dhcp.odhcpd
/etc/init.d/odhcpd disable
/etc/init.d/odhcpd stop

# ========================================================
# Commit changes, flush, and restart network
# ========================================================
# This way we will get internet on this AP and we must reconnect
uci commit
sync
/etc/init.d/network restart
