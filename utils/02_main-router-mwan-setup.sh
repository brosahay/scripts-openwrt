#!/bin/bash

# Switch configuration
uci add network switch_vlan
uci set network.@switch_vlan[-1].device='switch0'
uci set network.@switch_vlan[-1].vlan='3'
uci set network.@switch_vlan[0].ports='0t 2 3 4 5'
uci set network.@switch_vlan[0].vid='1'
uci set network.@switch_vlan[1].ports='0t 1'
uci set network.@switch_vlan[1].vid='2'

# Interface configuration
uci set network.wanb=interface
uci set network.wanb.proto='dhcp'
uci set network.wanb.device='eth0.3'
uci add_list firewall.@zone[1].network='wanb'

# WANA configuration
uci set network.wan.proto='static'
uci set network.wan.ipaddr='10.16.239.74'
uci set network.wan.netmask='255.255.255.128'
uci set network.wan.gateway='10.16.239.1'
uci set network.wan.broadcast='10.16.239.127'
uci add_list network.wan.dns='10.16.239.1'
uci add_list network.wan.dns='8.8.8.8'
uci set network.wan.metric='10'

# WANB configuration
uci set network.wanb.proto='static'
uci set network.wanb.ipaddr='172.28.66.222'
uci set network.wanb.netmask='255.255.255.0'
uci set network.wanb.gateway='172.28.66.1'
uci set network.wanb.broadcast='172.28.66.255'
uci add_list network.wanb.dns='172.17.6.1'
uci add_list network.wanb.dns='8.8.8.8'
uci set network.wanb.metric='20'

# ========================================================
# Commit changes, flush, and restart network
# ========================================================
uci commit
sync
/etc/init.d/network restart

# ========================================================
# Clean up
# ========================================================
uci del mwan3.wan6
uci del mwan3.wanb6
