#!/bin/bash
set -e

YAML_FILE=$1
FILES=$2

export HOSTNAME=$(yq eval '.configure.network.system.hostname' "${YAML_FILE}")
export LAN_HOSTNAME=$(yq eval '.configure.network.lan.hostname' "${YAML_FILE}")
export SSID=$(yq eval '.configure.wireless.wifi-iface.ssid' "${YAML_FILE}")
export SEC_MODE=$(yq eval '.configure.wireless.wifi-iface.encryption' "${YAML_FILE}")
export PSK=$(yq eval '.configure.wireless.wifi-iface.key' "${YAML_FILE}")
export WAN6_PROTO=$(yq eval '.configure.network.wan6.proto' "${YAML_FILE}")
export LAN_TYPE=$(yq eval '.configure.network.lan.type' "${YAML_FILE}")
export LAN_PROTO=$(yq eval '.configure.network.lan.proto' "${YAML_FILE}")
export LAN_IPADDR=$(yq eval '.configure.network.lan.ipaddr' "${YAML_FILE}")
export LAN6_PROTO=$(yq eval '.configure.network.lan6.proto' "${YAML_FILE}")
export PKT_STEERING=$(yq eval '.configure.network.globals.packet_steering' "${YAML_FILE}")

CONFIG_FILE="$FILES/etc/uci-defaults"
rm -rf $CONFIG_FILE
mkdir -p $CONFIG_FILE

CONFIG_FILE="$CONFIG_FILE/99-uci-defaults"
touch $CONFIG_FILE
chmod 755 $CONFIG_FILE

cat <<EOF >>$CONFIG_FILE
$(sh ./scripts/00_pre-init.sh)
$(sh ./scripts/00_system-openwrt.sh)
$(sh ./scripts/01_wlan-factory.sh)
$(sh ./scripts/02_network-switch.sh)
$(sh ./scripts/03_network-wan6-disable.sh)
$(sh ./scripts/05_network-lan-dhcp.sh)
$(sh ./scripts/05_network-lan-static.sh)
$(sh ./scripts/05_network-lan6-disable.sh)
$(sh ./scripts/05_network-lan6-slaac.sh)
$(sh ./scripts/10_packet-steering.sh)
$(sh ./scripts/99_post-init.sh)
EOF

unset TARGET
unset PROFILE
unset HOSTNAME
unset LAN_HOSTNAME
unset SSID
unset SEC_MODE
unset PSK
unset WAN6_PROTO
unset LAN_TYPE
unset LAN_PROTO
unset LAN_IPADDR
unset LAN6_PROTO
unset PKT_STEERING
