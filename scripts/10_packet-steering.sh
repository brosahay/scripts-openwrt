#!/bin/bash

# export PKT_STEERING

if [ -z "${PKT_STEERING}" ] || [ "${PKT_STEERING}" = "null" ] || [ "${PKT_STEERING}" = "0" ]; then
  exit 0
fi

cat <<EOF
## ENABLE PACKET STEERING
uci set network.globals.packet_steering='1'
EOF
