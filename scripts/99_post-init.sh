#!/bin/bash

cat <<EOF
uci commit

/etc/init.d/network restart
$(
  if [ ! -z "${SSID}" ] && [ "${SSID}" != "null" ]; then
    echo wifi reload
  fi
)
exit 0 # IMPORTANT, IF WE NO PUT THIS, WILL EXECUTED ENDLESSLY
EOF
