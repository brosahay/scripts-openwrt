#!/bin/bash

# export WAN6_PROTO

if [ ! -z "${WAN6_PROTO}" ] || [ "${WAN6_PROTO}" != "null" ]; then
  exit 0
fi

cat <<EOF
## DISABLE IPV6 WAN
uci -q del network.wan6
uci -q del_list firewall.@zone[1].network='wan6'
EOF
