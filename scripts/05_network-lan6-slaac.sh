#!/bin/bash

# export LAN6_PROTO

if [ "${LAN6_PROTO}" != "slaac" ]; then
  exit 0
fi

cat <<EOF
## ENABLE IPV6 LAN SLAAC
uci -q set dhcp.lan.ra='server'
uci -q set dhcp.lan.ra_slaac=1
uci -q del dhcp.lan.ra_flags
uci -q set dhcp.lan.ra_flags='none'
EOF
