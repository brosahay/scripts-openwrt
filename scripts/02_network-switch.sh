#!/bin/bash

# export LAN_TYPE

if [ "${LAN_TYPE}" != "bridge" ]; then
  exit 0
fi

cat <<EOF
## SWITCH CONFIGURATION
# set lan logical interface as bridge (to allow bridge multiple physical interfaces)
uci set network.lan.type='bridge'
if [ "\$(uci -q show network.wan.device)" != "" ]; then
  # assign WAN physical interface to LAN (will be available as an additional LAN port now)
  uci -q set network.lan.device="\$(uci -q get network.lan.device) \$(uci -q get network.wan.device)"
  uci -q del network.wan.device
  # Remove wan logical interface, since we will not need it.
  uci -q del network.wan
fi
EOF
