#!/bin/bash

cat <<EOF
#!/bin/bash

if [ "\$(uci -q get system.@system[0].init)" != "" ]; then
  exit 0
fi
uci set system.@system[0].init='initiated'
uci commit
if [ -e /etc/init ]; then
  exit 0
fi
touch /etc/init
EOF
