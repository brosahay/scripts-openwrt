#!/bin/bash

# export LAN6_PROTO

if [ "${LAN6_PROTO}" == "slaac" ]; then
  exit 0
fi

cat <<EOF
## DISABLE IPV6 LAN
uci -q del network.lan.ip6assign
uci -q set network.lan.delegate='0'
uci -q del network.globals.ula_prefix
uci -q del dhcp.lan.dhcpv6
uci -q del dhcp.lan.ra
uci -q del dhcp.odhcpd
if [ -e /etc/init.d/odhcpd ]; then
  /etc/init.d/odhcpd disable
  /etc/init.d/odhcpd stop 2 &>/dev/null
fi
EOF
