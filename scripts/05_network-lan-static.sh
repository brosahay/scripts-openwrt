#!/bin/bash

if [ -z "${LAN_PROTO}" ] || [ "${LAN_PROTO}" = "null" ] || [ "${LAN_PROTO}" != "static" ]; then
  exit 0
fi

# export LAN_IPADDR
# export LAN_DOMAIN

cat <<EOF
## IPV4 LAN STATIC DEFAULTS
uci -q set dhcp.@dnsmasq[0].local='/home.arpa/'
uci -q set dhcp.@dnsmasq[0].domain='home.arpa'
$(
  if [ ! -z "${LAN_IPADDR}" ] && [ "${LAN_IPADDR}" != "null" ]; then
    echo uci -q set network.lan.ipaddr=\'"${LAN_IPADDR}"\'
  fi
)
EOF
# uci set network.lan.broadcast=\$(uci -q show network.lan.ipaddr | awk -F= '{ print \$2 }' | awk -F. '{print \$1 "." \$2 "." \$3 ".255"}')
