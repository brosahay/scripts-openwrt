#!/bin/bash

# export SSID
# export SEC_MODE none, psk2+aes
# export PSK

if [ -z "${SSID}" ] || [ "${SSID}" = "null" ]; then
  exit 0
fi

cat <<EOF
## DEFAULT WIFI CONFIGURATION
IDX=0
while [ "\$(uci -q show wireless.@wifi-device[\$IDX])" != "" ]; do
  BAND=\$(uci show wireless.@wifi-device[\$IDX].band | awk -F= '{ print \$2 }' | awk -F\' '{print \$2}' | tr 'a-z' 'A-Z')
  IFACE_SSID=${SSID}
  # ADD BAND
  if [ "\${BAND}" != "2G" ]; then
    # SKIP IF 2G
    IFACE_SSID="\${IFACE_SSID}_\${BAND}"
  fi
  # ADD INTERFACE NUMBER
  if [ \$IDX -gt 1 ]; then
    IFACE_SSID="\$IFACE_SSID_\$(IDX-1)"
  fi
  uci set wireless.@wifi-iface[\$IDX].ssid="${SSID}"
  uci set wireless.@wifi-iface[\$IDX].encryption="${SEC_MODE}"
  $(
  if [ "${SEC_MODE}" != "none" ]; then
    echo uci set wireless.@wifi-iface[\$IDX].key="${PSK}"
  fi
)
  uci set wireless.@wifi-device[\$IDX].channel='auto'
  uci set wireless.@wifi-device[\$IDX].disabled=0
  IDX=\$((IDX + 1))
done
EOF
