#!/bin/bash

# export HOSTNAME
# export LAN_HOSTNAME
# uci set system.@system[0].hostname="\$(cat /etc/board.json | grep name | cut -d : -f2 | awk -F\" '{print \$2}')"

cat <<EOF
## OPENWRT SYSTEM
uci set system.@system[0].zonename='Asia/Kolkata'
uci set system.@system[0].timezone='IST-5:30'
$(
  if [ ! -z "${HOSTNAME}" ] && [ "${HOSTNAME}" != "null" ]; then
    echo uci set system.@system[0].hostname=\"${HOSTNAME}\"
  fi
)
$(
  if [ ! -z "${LAN_HOSTNAME}" ] && [ "${LAN_HOSTNAME}" != "null" ]; then
    echo uci set network.lan.hostname=\'"${LAN_HOSTNAME}"\'
  fi
)
EOF
