#!/bin/bash

if [ -z "${LAN_PROTO}" ] || [ "${LAN_PROTO}" = "null" ] || [ "${LAN_PROTO}" != "dhcp" ]; then
  exit 0
fi

cat <<EOF
## IPV4 LAN DHCP DEFAULTS
# set DHCP on LAN (not recommended, but useful when Dumb AP is moveable from one building to another)
uci -q del network.lan.broadcast
uci -q del network.lan.dns
uci -q del network.lan.gateway
uci -q del network.lan.ipaddr
uci -q del network.lan.netmask
uci -q set network.lan.proto='dhcp'
# disable Dnsmasq completely (it is important to commit or discard dhcp)
uci -q commit dhcp; echo '' >/etc/config/dhcp
if [ -e /etc/init.d/dnsmasq ]; then
  /etc/init.d/dnsmasq disable
  /etc/init.d/dnsmasq stop 2 &>/dev/null
fi
EOF
