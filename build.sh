#!/bin/bash
set -e

# export IMAGEBUILDER_BIN

RELEASE="21.02.7"
FILES="$PWD/files"

usage() {
  cat <<-HELP
usage: $0 [options]
  -T IMAGEBUILDER_BIN       path to imagebuilder ($IMAGEBUILDER_BIN)
  -t TARGET                 build for a specific target ($TARGET)
  -p PROFILE                build a specific profile ($PROFILE)
  -P PACKAGES               include packages in firmware ($PACKAGES)
  -c CONFIG_NAME            provide configname (check configs folder)
  -l                        only list availble profiles for target
  -d                        dry run openwrt manifest and uci-defaults
  -g                        generate uci-defaults
HELP
}

[[ $1 == --help ]] && {
  usage
  exit 0
}
while getopts "hr:T:t:p:P:c::ldg" opt; do
  case "$opt" in
  T) IMAGEBUILDER_BIN=$OPTARG ;;
  t) TARGET=$OPTARG ;;
  p) PROFILE=$OPTARG ;;
  P) PACKAGES=$OPTARG ;;
  l) LISTONLY=yes ;;
  d) DRY_RUN=yes ;;
  g) GENERATE_ONLY=yes ;;
  c) CONFIG_NAME=$OPTARG ;;
  h)
    usage
    exit 0
    ;;
  \?) exit 1 ;;
  esac
done

TARGET=$(yq eval '.target' "${CONFIG_NAME}")
PROFILE=$(yq eval '.profile' "${CONFIG_NAME}")

if [ -z "${IMAGEBUILDER_BIN}" ]; then
  IMAGEBUILDER_BIN="$PWD/openwrt-imagebuilder-$RELEASE-$TARGET.Linux-x86_64"
fi

if [ ! -d "${IMAGEBUILDER_BIN}" ]; then
  echo "ERROR: PROVIDE IMAGEBUILDER_BIN"
  exit 1
fi

if [[ $LISTONLY == yes ]]; then
  make --directory="${IMAGEBUILDER_BIN}" info
  exit 0
fi

bash ./generate-uci-defaults.sh "${CONFIG_NAME}" "${FILES}"

if [[ $GENERATE_ONLY == yes ]]; then
  exit 0
fi

PKGS=$(yq eval '.packages.del[]' "${CONFIG_NAME}" | sed -e 's/^/-/')
PKGS+=$(yq eval '.packages.add[]' "${CONFIG_NAME}")

make --directory="${IMAGEBUILDER_BIN}" clean

if [[ $DRY_RUN == yes ]]; then
  make --directory="${IMAGEBUILDER_BIN}" manifest \
    PROFILE="$PROFILE" \
    PACKAGES="${PKGS} ${PACKAGES}"
  exit 0
fi

make --directory="${IMAGEBUILDER_BIN}" image \
  PROFILE="$PROFILE" \
  FILES="$FILES" \
  BIN_DIR="${PWD}/images" \
  PACKAGES="${PKGS} ${PACKAGES}"
