# Example configuration

### minimal image for a DUMB access point (Recommended) with DAWN

```sh
PACKAGES="-dnsmasq -firewall -ip6tables -iptables -odhcpd-ipv6only -ppp -ppp-mod-pppoe -opkg -uclient-fetch -odhcp6c -luci -kmod-ipt-offload -wpad-basic-wolfssl uhttpd luci-mod-admin-full luci-base libiwinfo-lua luci-theme-bootstrap wpad-wolfssl dawn"
```

### minimal image for a DUMB access point without DAWN

```sh
PACKAGES="-dnsmasq -firewall -ip6tables -iptables -odhcpd-ipv6only -ppp -ppp-mod-pppoe -opkg -uclient-fetch -odhcp6c -luci -kmod-ipt-offload uhttpd luci-mod-admin-full luci-base libiwinfo-lua luci-theme-bootstrap"
```

### minimal image for a DUMB access point

```sh
PACKAGES="-luci-app-firewall -ppp -ppp-mod-pppoe -ip6tables -odhcp6c -kmod-ipv6 -kmod-ip6tables -odhcpd -iptables -odhcpd-ipv6only -wpad-mini -wpad-basic-wolfssl -dnsmasq -firewall -kmod-ipt-conntrack -kmod-ipt-core -kmod-ipt-nat -kmod-ipt-offload -dnsmasq -firewall -kmod-nf-conntrack -kmod-nf-conntrack6 -kmod-nf-flow -kmod-nf-ipt -kmod-nf-nat -kmod-nf-reject -ca-bundle -libwolfssl24 uhttpd uhttpd-mod-ubus libiwinfo-lua luci-base luci-mod-admin-full luci-theme-bootstrap wpad-wolfssl dawn"
```

### minimal-luci-install equivalent
```sh
PACKAGES="-luci uhttpd luci-mod-admin-full luci-base libiwinfo-lua luci-theme-bootstrap"
```
