# INSTALLATION

## Run the docker development container
```sh
docker-compose up -d
```

This will do the following:
 - create docker image if it is not present
 - create docker volume for persistent storage of the git repository of openWRT.
 - first time it will pull in the repository for openWRT.

# CONFIGURATION
Modify the `TAG_NAME` in `entrypoint.sh` in order to build some other version of openWRT.

```sh
...
TAG_NAME="v22.03.3"
...
```
Remove the volume and rebuild the docker image
```sh
docker build -t openwrt-build:latest .
```
